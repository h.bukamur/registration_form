import '../constant/gender.dart';

class User {
  User(
      {this.fullName,
      this.phoneNumber,
      this.email,
      this.gender = Gender.male,
      this.occupation,
      this.dOB});

  String? fullName;
  String? phoneNumber;
  String? email;
  Gender gender;
  String? occupation;
  DateTime? dOB;
}
