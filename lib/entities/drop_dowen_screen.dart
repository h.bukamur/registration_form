import 'package:flutter/material.dart';
import 'package:country_picker/country_picker.dart' as c;

class DropDownScreen extends StatefulWidget {
  const DropDownScreen({Key? key}) : super(key: key);

  @override
  State<DropDownScreen> createState() => _DropDownScreenState();
}

class _DropDownScreenState extends State<DropDownScreen> {
  var countries = [
    Country(
        id: 1,
        name: "Libya",
        flag: "https://www.worldometers.info/img/flags/ly-flag.gif",
        countryCode: "218"),
    Country(
        id: 2,
        name: "Tunis",
        flag: "https://www.worldometers.info/img/flags/ts-flag.gif",
        countryCode: "216"),
    Country(
        id: 3,
        name: "Egypt",
        flag: "https://www.worldometers.info/img/flags/eg-flag.gif",
        countryCode: "20"),
    Country(
        id: 4,
        name: "Algeria",
        flag: "https://www.worldometers.info/img/flags/ag-flag.gif",
        countryCode: "212"),
    Country(
        id: 5,
        name: "Morocco",
        flag: "https://www.worldometers.info/img/flags/mo-flag.gif",
        countryCode: "212"),
  ];
  late Country selectedCountry = countries[0];
  late Country selectedCountry2 = countries[0];
  Status status = Status.newOrder;
  c.Country? selectedCountry3;

  @override
  Widget build(BuildContext context) {
    countries.sort((Country a, Country b) => a.name.compareTo(b.name));
    return Scaffold(
      appBar: AppBar(
        title: const Text("Drop Down Screen"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DropdownButton(
                value: selectedCountry,
                items: countries.map<DropdownMenuItem<Country>>((Country e) {
                  return DropdownMenuItem<Country>(
                    value: e,
                    child: Text(
                      "${e.id}: ${e.name} ",
                      style: const TextStyle(
                        fontSize: 30,
                      ),
                    ),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    selectedCountry = value!;
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              DropdownButton(
                value: status,
                items: Status.values.map<DropdownMenuItem<Status>>((Status e) {
                  return DropdownMenuItem<Status>(
                    value: e,
                    child: Text(
                      e.name,
                      style: const TextStyle(
                        fontSize: 30,
                      ),
                    ),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    status = value!;
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 20,
                  ),
                  decoration: BoxDecoration(
                    // color: Colors.black26,
                    border: Border.all(
                      color: Colors.black,
                    ),
                    borderRadius: BorderRadius.circular(
                      20,
                    ),
                  ),
                  width: double.maxFinite,
                  child: DropdownButton(
                    value: selectedCountry2,
                    isExpanded: true,
                    underline: Container(),
                    items: countries
                        .map<DropdownMenuItem<Country>>((Country country) {
                      return DropdownMenuItem<Country>(
                        value: country,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "(${country.countryCode}) ${country.name}",
                              style: const TextStyle(
                                fontSize: 20,
                              ),
                            ),
                            Image.network(
                              country.flag,
                              width: 60,
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        selectedCountry2 = value!;
                      });
                    },
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: pickCountry,
                child: Text(
                  selectedCountry3 == null
                      ? "Select Country"
                      : "${selectedCountry3!.flagEmoji}  ${selectedCountry3!.name}",
                  style: const TextStyle(fontSize: 28),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void pickCountry() {
    c.showCountryPicker(
        context: context,
        countryListTheme: c.CountryListThemeData(
          flagSize: 40,
          backgroundColor: Colors.white,
          textStyle: const TextStyle(fontSize: 20, color: Colors.blueGrey),
          bottomSheetHeight: MediaQuery.of(context).size.height -
              (MediaQuery.of(context).size.height / 6),
          // Optional. Country list modal height
          //Optional. Sets the border radius for the bottomsheet.
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
          //Optional. Styles the search field.
          inputDecoration: InputDecoration(
            labelText: 'Search',
            hintText: 'Start typing to search',
            prefixIcon: const Icon(Icons.search),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(
                20,
              ),
              borderSide: BorderSide(
                color: const Color(0xFF8C98A8).withOpacity(0.2),
              ),
            ),
          ),
        ),
        onSelect: (c.Country country) {
          setState(() {
            selectedCountry3 = country;
            //print(selectedCountry3!.flagEmoji);
          });
        });
  }
}

class Country {
  Country(
      {required this.id,
      required this.name,
      required this.flag,
      required this.countryCode});

  int id;
  String name;
  String countryCode;
  String flag;
}

enum Status { newOrder, pending, canceled, complete }
