class Visit {
  Visit(
      {this.id,
      this.name,
      this.directorate,
      this.municipality,
      this.rating,
      this.visitDate});

  int? id;
  String? name;
  String? municipality;
  String? directorate;
  DateTime? visitDate;
  double? rating;
}
