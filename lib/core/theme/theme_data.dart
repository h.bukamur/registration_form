import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeDataProvider extends ChangeNotifier{
  ThemeMode themeMode = ThemeMode.system;

  bool get isDarkMode {
    if (themeMode == ThemeMode.system) {
      final brightness = SchedulerBinding.instance.platformDispatcher.platformBrightness;
      return brightness == Brightness.dark;
    } else {
      return themeMode == ThemeMode.dark;
    }
  }

  Future<void> toggleTheme(bool isOn) async {
    var prefs = await SharedPreferences.getInstance();
    themeMode = isOn ? ThemeMode.dark : ThemeMode.light;
    await prefs.setString("theme_mode", themeMode.name.toLowerCase());
    notifyListeners();
  }


  fetchThemeMode() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString('theme_mode') == null) {
      themeMode = ThemeMode.system;
      return Null;
    }
    if (kDebugMode) {
      print(prefs.getString('theme_mode'));
    }
    themeMode =
    prefs.getString('theme_mode') == ThemeMode.dark.name.toLowerCase()
        ? ThemeMode.dark
        : ThemeMode.light;
    return Null;
  }
}

const primaryColor =  Color(0xFF27ABBE);
const secondaryColorDark =  Color(0xff212124);
const secondaryColorLight =  Color(0xFFFFFFFF);
const accentColor =  Color(0xFFF8BD30);
const primaryColorDark = Color(0xFF3B6077);

final darkTheme = ThemeData(
  fontFamily: 'Almarai',
  appBarTheme: const AppBarTheme(
    toolbarHeight: 80,
    backgroundColor: secondaryColorDark,
    centerTitle: true,
    iconTheme: IconThemeData(color: accentColor, opacity: 0.8),
    titleTextStyle: TextStyle(
      fontFamily: 'Almarai',
      color: primaryColor,
      fontWeight: FontWeight.bold,
      fontSize: 20,
    ),
  ),
  brightness: Brightness.dark,
  primaryColor: primaryColor,
  scaffoldBackgroundColor: secondaryColorDark,
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    selectedIconTheme: IconThemeData(color: primaryColor, opacity: 0.8),
    unselectedIconTheme:
    IconThemeData(color: secondaryColorLight, opacity: 0.8),
    selectedLabelStyle: TextStyle(fontSize: 16, color: secondaryColorLight),
    unselectedLabelStyle: TextStyle(fontSize: 14, color: secondaryColorLight),
    selectedItemColor: primaryColor,
    unselectedItemColor: secondaryColorLight,
  ),
  iconTheme: const IconThemeData(color: primaryColor, opacity: 0.8),
  colorScheme: const ColorScheme.dark(
    primary: primaryColor,
    secondary: accentColor,
  ),
  checkboxTheme: CheckboxThemeData(
    checkColor: MaterialStateProperty.all(secondaryColorLight),
    fillColor: MaterialStateProperty.all(primaryColor),
    side: const BorderSide(color: secondaryColorLight),
  ),
  switchTheme: SwitchThemeData(
    thumbColor: MaterialStateProperty.all(primaryColor),
    trackColor: MaterialStateProperty.all(secondaryColorLight),
  ),
  radioTheme:
  RadioThemeData(fillColor: MaterialStateProperty.all(primaryColor)),
  progressIndicatorTheme: const ProgressIndicatorThemeData(color: primaryColor),
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    // backgroundColor: primaryColor,
    foregroundColor: secondaryColorLight,
  ),
  inputDecorationTheme: InputDecorationTheme(
    floatingLabelStyle:  const TextStyle(color: primaryColor),
    iconColor: primaryColor,
    focusedBorder: OutlineInputBorder(
      borderSide:  const BorderSide(color: primaryColor),
      borderRadius: BorderRadius.circular(8),
    ),
    border: OutlineInputBorder(
      borderSide:  const BorderSide(color: primaryColor),
      borderRadius: BorderRadius.circular(8),
    ),
  ),
  dividerColor: secondaryColorLight,
  dividerTheme: const DividerThemeData(
    thickness: 1.5,
  ),
);

final lightTheme = ThemeData(
  fontFamily: 'Almarai',
  appBarTheme: const AppBarTheme(
    toolbarHeight: 80,
    iconTheme: IconThemeData(color: accentColor, opacity: 0.8),
    backgroundColor: primaryColor,
    centerTitle: true,
    titleTextStyle: TextStyle(
      fontFamily: 'Almarai',
      color: secondaryColorLight,
      fontWeight: FontWeight.bold,
      fontSize: 20,
    ),
  ),
  brightness: Brightness.light,
  primaryColor: primaryColor,
  colorScheme: const ColorScheme.light(
    primary: primaryColor,
    secondary: accentColor,
  ),
  // scaffoldBackgroundColor: secondaryColorLight,
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    selectedIconTheme: IconThemeData(color: primaryColor, opacity: 0.8),
    unselectedIconTheme: IconThemeData(color: secondaryColorDark, opacity: 0.8),
    selectedLabelStyle: TextStyle(fontSize: 16, color: secondaryColorDark),
    unselectedLabelStyle: TextStyle(fontSize: 14, color: secondaryColorDark),
    selectedItemColor: primaryColor,
    unselectedItemColor: secondaryColorDark,
  ),
  iconTheme: const IconThemeData(color: primaryColor, opacity: 0.8),
  checkboxTheme: CheckboxThemeData(
    checkColor: MaterialStateProperty.all(secondaryColorLight),
    fillColor: MaterialStateProperty.all(primaryColor),
    side: const BorderSide(color: secondaryColorDark),
  ),
  switchTheme: SwitchThemeData(
    thumbColor: MaterialStateProperty.all(primaryColor),
    trackColor: MaterialStateProperty.all(secondaryColorDark),
  ),
  radioTheme:
  RadioThemeData(fillColor: MaterialStateProperty.all(primaryColor)),
  progressIndicatorTheme: const ProgressIndicatorThemeData(color: primaryColor),
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    // backgroundColor: primaryColor,
    foregroundColor: secondaryColorLight,
  ),
  inputDecorationTheme: InputDecorationTheme(
    floatingLabelStyle: const TextStyle(color: primaryColor),
    iconColor: primaryColor,
    focusedBorder: OutlineInputBorder(
      borderSide: const BorderSide(color: primaryColor),
      borderRadius: BorderRadius.circular(8),
    ),
    border: OutlineInputBorder(
      borderSide: const BorderSide(color: primaryColor),
      borderRadius: BorderRadius.circular(8),
    ),
  ),
  dividerColor: secondaryColorDark,
  dividerTheme: const DividerThemeData(
    thickness: 1.5,
  ),
);