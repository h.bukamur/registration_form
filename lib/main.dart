import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:registration_form/providers/visit_provider.dart';
import 'package:registration_form/screens/card_screen.dart';
import 'package:registration_form/screens/language_example.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/theme/theme_data.dart';
import 'core/util/applanguage.dart';
import 'core/util/localizations.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();
  final ThemeDataProvider themeDataProvider = ThemeDataProvider();
  themeDataProvider.fetchThemeMode();
  runApp(ChangeNotifierProvider<AppLanguage>.value(
      value: appLanguage, child: const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late SharedPreferences prefs;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ThemeDataProvider(),
      builder: (context, _) {
        final themeProvider =
        Provider.of<ThemeDataProvider>(context, listen: true);
        return ChangeNotifierProvider(
            create: (context) => VisitProvider(),
            builder: (context, _) {
              return Consumer<ThemeDataProvider>(
                builder: (ctx, snapShot, child) =>
                    Consumer<AppLanguage>(
                      builder: (ctx, snapShot, child) =>
                          MaterialApp(
                            home: const LanguageExample(),
                            debugShowCheckedModeBanner: false,
                            supportedLocales: const [
                              Locale('ar'),
                              Locale('en')
                            ],
                            localizationsDelegates: const [
                              AppLocalizations.delegate,
                              GlobalMaterialLocalizations.delegate,
                              GlobalCupertinoLocalizations.delegate,
                              GlobalWidgetsLocalizations.delegate,
                            ],
                            locale: snapShot.appLocal.languageCode == "ar"
                                ? const Locale('ar')
                                : snapShot.appLocal,
                            theme: lightTheme,
                            themeMode: themeProvider.themeMode,
                            darkTheme: darkTheme,
                          ),
                    ),
              );
            });
      },
    );
  }
}
