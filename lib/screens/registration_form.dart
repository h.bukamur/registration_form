import 'package:flutter/material.dart';
import 'package:registration_form/screens/registration_view.dart';

import '../constant/gender.dart';
import '../entities/user.dart';


class RegistrationForm extends StatefulWidget {
  RegistrationForm({Key? key}) : super(key: key);

  @override
  State<RegistrationForm> createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<RegistrationForm> {
  User _user = User();
  bool _validateFullName = false;
  bool _validatephoneNumber = false;
  bool _validateEmail = false;
  TextEditingController fullNameController = TextEditingController();

  TextEditingController phoneNumberController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  void _signup() {
    if (fullNameController.text.isEmpty) {
      setState(() {
        _validateFullName = true;
      });
      return;
    }
    if (phoneNumberController.text.isEmpty) {
      setState(() {
        _validatephoneNumber = true;
      });
      return;
    }
    if (emailController.text.isEmpty) {
      setState(() {
        _validateEmail = true;
      });
      return;
    }

    _user.fullName = fullNameController.text;
    _user.phoneNumber = phoneNumberController.text;
    _user.email = emailController.text;
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => RegistrationView(
          user: _user,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registration Form'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // full name
            TextField(
              controller: fullNameController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                label: const Text('Full Name'),
                errorText:
                    _validateFullName ? 'Please enter full name!.' : null,
              ),
              onChanged: (value) {
                if (value.isNotEmpty) {
                  setState(() {
                    _validateFullName = false;
                  });
                } else {
                  setState(() {
                    _validateFullName = true;
                  });
                }
              },
            ),
            const SizedBox(
              height: 20,
            ),
            // phone number
            TextField(
              controller: phoneNumberController,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                label: const Text('Phone Number'),
                errorText:
                    _validatephoneNumber ? 'Please enter phone number!.' : null,
              ),
              onChanged: (value) {
                if (value.isNotEmpty) {
                  setState(() {
                    _validatephoneNumber = false;
                  });
                } else {
                  setState(() {
                    _validatephoneNumber = true;
                  });
                }
              },
            ),
            const SizedBox(
              height: 20,
            ),
            // email
            TextField(
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                label: const Text('Email'),
                errorText:
                    _validateEmail ? 'Please enter email address!.' : null,
              ),
              onChanged: (value) {
                if (value.isNotEmpty) {
                  setState(() {
                    _validateEmail = false;
                  });
                } else {
                  setState(() {
                    _validateEmail = true;
                  });
                }
              },
            ),
            const SizedBox(
              height: 20,
            ),
            // gender
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: RadioListTile(
                      title: const Text('Male'),
                      value: Gender.male,
                      groupValue: _user.gender,
                      selected: true,
                      onChanged: (value) {
                        setState(() {
                          _user.gender = value!;
                        });
                      }),
                ),
                Expanded(
                  child: RadioListTile(
                      title: const Text('Female'),
                      value: Gender.female,
                      groupValue: _user.gender,
                      selected: false,
                      onChanged: (value) {
                        setState(() {
                          _user.gender = value!;
                        });
                      }),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: double.maxFinite,
              height: 50,
              child: ElevatedButton(
                onPressed: _signup,
                child: const Text(
                  'Signup',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
