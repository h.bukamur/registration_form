import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Application'),
      ),
      body: const Center(
        child: Text(
          'Welcome to our application.',
          style: TextStyle(
            fontSize: 28,
          ),
        ),
      ),
    );
  }
}
