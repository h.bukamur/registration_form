import 'package:flutter/material.dart';

class RecievedScreen extends StatelessWidget {
  const RecievedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(child: Icon(Icons.call_received));
  }
}
