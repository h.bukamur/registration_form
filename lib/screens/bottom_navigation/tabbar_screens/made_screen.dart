import 'package:flutter/material.dart';

class MadeScreen extends StatelessWidget {
  const MadeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(child: Icon(Icons.call_made));
  }
}
