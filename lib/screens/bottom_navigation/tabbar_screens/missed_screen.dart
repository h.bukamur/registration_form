import 'package:flutter/material.dart';

class MissedScreen extends StatelessWidget {
  const MissedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(child: Icon(Icons.call_missed));
  }
}
