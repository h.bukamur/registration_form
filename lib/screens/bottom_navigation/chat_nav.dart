import 'package:flutter/material.dart';

class ChatNav extends StatelessWidget {
  const ChatNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Chat Screen')),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.chat),
      ),
      body: const Center(
        child: Icon(
          Icons.chat,
          size: 48,
        ),
      ),
    );
  }
}
