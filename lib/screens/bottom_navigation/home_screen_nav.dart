import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart' as ph;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class HomeNav extends StatefulWidget {
  HomeNav({Key? key}) : super(key: key);

  @override
  State<HomeNav> createState() => _HomeNavState();
}

class _HomeNavState extends State<HomeNav> {
  XFile? file;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Home Screen')),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
      body: Center(
          child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          file == null
              ? Container(
                  height: 400,
                  width: 300,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                  ),
                  child: const Center(
                    child: Text(
                      'Picked Image',
                    ),
                  ),
                )
              : SizedBox(
                  height: 400,
                  child: Image.file(
                    File(file!.path),
                  ),
                ),
          ElevatedButton(
            onPressed: _pickImage,
            child: const Text(
              'Select Media',
            ),
          ),
        ],
      )),
    );
  }

  Future<ph.PermissionStatus> requestCameraPermission() async {
    return await ph.Permission.camera.request();
  }

  Future<ph.PermissionStatus> requestPhotosPermission() async {
    return await ph.Permission.photos.request();
  }

  Future<ph.PermissionStatus> requestStoragePermission() async {
    return await ph.Permission.storage.request();
  }

  Future<void> _pickImage() async {
    var storagePermissionStatus = await requestStoragePermission();
    if (storagePermissionStatus == ph.PermissionStatus.denied) {
      Fluttertoast.showToast(
          msg: "Storage Permission Is Required!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 5,
          backgroundColor: Colors.grey,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }


    var cameraPermissionStatus = await requestCameraPermission();
    if (cameraPermissionStatus == ph.PermissionStatus.denied) {
      Fluttertoast.showToast(
          msg: "Camera Permission Is Required!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 5,
          backgroundColor: Colors.grey,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }

    if (!context.mounted) return;

    showDialog<ImageSource>(
      context: context,
      builder: (context) => AlertDialog(
        content: const Text(
          "Choose Image Source",
        ),
        actions: [
          TextButton(
            child: const Text("Camera"),
            onPressed: () => Navigator.pop(context, ImageSource.camera),
          ),
          TextButton(
            child: const Text("Gallery"),
            onPressed: () => Navigator.pop(context, ImageSource.gallery),
          ),
        ],
      ),
    ).then((ImageSource? source) async {
      if (source != null) {
        final imageFile = await ImagePicker().pickImage(source: source);
        if (imageFile == null) {
          return;
        }
        setState(() {
          file = imageFile;
        });
      }else{
        Fluttertoast.showToast(
            msg: "No Source Selected!",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 5,
            backgroundColor: Colors.grey,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    });
  }
}
