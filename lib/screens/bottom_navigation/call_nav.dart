import 'package:flutter/material.dart';
import 'package:registration_form/screens/bottom_navigation/tabbar_screens/made_screen.dart';
import 'package:registration_form/screens/bottom_navigation/tabbar_screens/missed_screen.dart';
import 'package:registration_form/screens/bottom_navigation/tabbar_screens/recieved_screen.dart';

class CallNav extends StatefulWidget {
  const CallNav({Key? key}) : super(key: key);

  @override
  State<CallNav> createState() => _CallNavState();
}

class _CallNavState extends State<CallNav> {


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      initialIndex: 0,
      child: Scaffold(
          appBar: AppBar(
            title: const Center(child: Text('Call Screen')),
            bottom: const TabBar(
              tabs: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('Outgoing'),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('Incoming'),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('Missed'),
                ),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {},
            child: const Icon(Icons.call),
          ),
          body:   const TabBarView(
            children: [
              MadeScreen(),
              RecievedScreen(),
              MissedScreen(),
            ],
          )),
    );
  }
}
