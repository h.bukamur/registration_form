import 'package:flutter/material.dart';

import 'call_nav.dart';
import 'chat_nav.dart';
import 'home_screen_nav.dart';

class HomeScreenNave extends StatefulWidget {
  const HomeScreenNave({Key? key}) : super(key: key);

  @override
  State<HomeScreenNave> createState() => _HomeScreenNaveState();
}

class _HomeScreenNaveState extends State<HomeScreenNave> {
  int _selectedIndex = 0;

  final List<Widget> _pages = [
    HomeNav(),
    const CallNav(),
    const ChatNav(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Home Navigation'),
      // ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: (){},
      //   child: const Icon(Icons.add),
      // ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: (value) {
          setState(() {
            _selectedIndex = value;
          });
        },
        selectedIconTheme: const IconThemeData(
          size: 36,
          color: Colors.deepOrangeAccent,
        ),
        unselectedIconTheme: const IconThemeData(
          size: 24,
          color: Colors.blue,
        ),
        selectedFontSize: 20,
        unselectedFontSize: 16,
        // selectedLabelStyle: const TextStyle(fontWeight: FontWeight.bold),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.call), label: 'Call'),
          BottomNavigationBarItem(icon: Icon(Icons.chat), label: 'Chat'),
        ],
      ),
      body: _pages[_selectedIndex],
    );
  }
}
