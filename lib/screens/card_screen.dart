import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:registration_form/constant/event_status.dart';
import '../providers/visit_provider.dart';
import '../widgets/visit_card_widget.dart';

class CardScreen extends StatelessWidget {
  const CardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<VisitProvider>(context, listen: false).getAll();
    return Scaffold(
      appBar: AppBar(
        title: const Text('New Lesson'),
      ),
      body: Consumer<VisitProvider>(
        builder: (BuildContext context, value, _) {
          if (value.status == EventStatus.loading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            if (value.status == EventStatus.error) {
              Fluttertoast.showToast(msg: "Error Occurred!");
              return const Center(
                child: Text('No Data.'),
              );
            } else {
              if (value.visits.isEmpty) {
                return const Center(
                  child: Text('No Data.'),
                );
              }
              return ListView.builder(
                itemCount: value.visits.length,
                itemBuilder: (context, index) {
                  return VisitCard(
                    visit: value.visits[index],
                  );
                },
              );
            }
          }
        },
      ),
      //VisitCard(visit: visit),
    );
  }
}
