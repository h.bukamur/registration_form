import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:registration_form/core/util/applanguage.dart';
import 'package:registration_form/core/util/string_extension.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/theme/theme_data.dart';
import '../core/util/localizations.dart';

class LanguageExample extends StatefulWidget {
  const LanguageExample({Key? key}) : super(key: key);

  @override
  State<LanguageExample> createState() => _LanguageExampleState();
}

class _LanguageExampleState extends State<LanguageExample> {
  @override
  Widget build(BuildContext context) {

    final themeProvider =
        Provider.of<ThemeDataProvider>(context, listen: false);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(
          Icons.add,
        ),
      ),
      appBar: AppBar(
        title: Text(
          'appName'.translate(context),
        ),
        actions: [
          IconButton(
            onPressed: () {
              if (themeProvider.themeMode == ThemeMode.dark) {
                themeProvider.toggleTheme(false);
              } else {
                themeProvider.toggleTheme(true);
              }
              setState(() {});
            },
            icon: themeProvider.themeMode == ThemeMode.dark
                ? const Icon(Icons.light_mode)
                : const Icon(Icons.dark_mode),
          ),
          IconButton(
            onPressed: () {
              var provider = Provider.of<AppLanguage>(context, listen: false);
              if (provider.appLocal == const Locale("ar")) {
                provider.changeLanguage(const Locale('en'));
              } else {
                provider.changeLanguage(const Locale('ar'));
              }
            },
            icon: const Icon(
              Icons.language,
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'data'.translate(context),
                style: const TextStyle(fontSize: 28),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 50,
                width: double.maxFinite,
                child: ElevatedButton(
                  onPressed: () {
                    var provider =
                        Provider.of<AppLanguage>(context, listen: false);
                    if (provider.appLocal == const Locale("ar")) {
                      provider.changeLanguage(const Locale('en'));
                    } else {
                      provider.changeLanguage(const Locale('ar'));
                    }
                  },
                  child: Text(
                    'changeLanguage'.translate(context),
                    style: const TextStyle(fontSize: 28),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
