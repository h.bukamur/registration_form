import 'package:flutter/material.dart';

import '../dialogs/settings_dialog.dart';

class ListViewExample extends StatelessWidget {
  const ListViewExample({Key? key}) : super(key: key);

  function({required BuildContext context}) {
    Navigator.of(context).pop();

    ///TO-DO actions....
    SettingsDialog().confirmationDialog(
        context: context,
        title: 'Confirmation',
        data: 'Data Saved Successfully!');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blueGrey,
                border: Border(
                  bottom: BorderSide(
                    color: Colors.grey[500]!,
                    width: 1.5,
                  ),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 110,
                    child: Image.asset(
                      "assets/images/logo.png",
                    ),
                  ),
                  const Text(
                    "App Name",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.language),
              title: const Text('Language'),
              onTap: () {
                SettingsDialog().confirmationDialog(
                    context: context,
                    title: 'Confirmation',
                    data: 'Data Saved Successfully!');
              },
            ),
            ListTile(
              leading: const Icon(Icons.settings),
              title: const Text('Settings'),
              onTap: () {
                SettingsDialog().showSettingsDialog(
                    context: context,
                    function: () {
                      function(context: context);
                    });
              },
            ),
            const Divider(
              thickness: 1.5,
            ),
            const AboutListTile(
              icon: Icon(Icons.account_balance_outlined),
            ),
            ListTile(
              leading: const Icon(Icons.language),
              title: const Text('Language'),
              onTap: () {},
            ),
            ListTile(
              leading: const Icon(Icons.settings),
              title: const Text('Settings'),
              onTap: () {},
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: const Text('List View'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.orangeAccent,
        elevation: 10,
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10.0,
          vertical: 10.0,
        ),
        child: ListView(
          children: [
            Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    20,
                  ),
                  side: const BorderSide(
                    color: Colors.blueGrey,
                  )),
              child: const ListTile(
                title: Text('Hatim Bukamur'),
                subtitle: Text('Software Engineer'),
                leading: Icon(
                  Icons.person,
                  color: Colors.orangeAccent,
                ),
                trailing: Icon(
                  Icons.ac_unit,
                  color: Colors.blue,
                ),
              ),
            ),
            const Divider(
              thickness: 1.5,
            ),
            Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    20,
                  ),
                  side: const BorderSide(
                    color: Colors.blueGrey,
                  )),
              child: const ListTile(
                title: Text('Saiid Mousa'),
                subtitle: Text('Software Engineer'),
                leading: Icon(
                  Icons.person,
                  color: Colors.orangeAccent,
                ),
                trailing: Icon(
                  Icons.ac_unit,
                  color: Colors.blue,
                ),
              ),
            ),
            const Divider(
              thickness: 1.5,
            ),
            Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    20,
                  ),
                  side: const BorderSide(
                    color: Colors.blueGrey,
                  )),
              child: const ListTile(
                title: Text('Abdulaziz Felfel'),
                subtitle: Text('Operation Manager'),
                leading: Icon(
                  Icons.person,
                  color: Colors.orangeAccent,
                ),
                trailing: Icon(
                  Icons.ac_unit,
                  color: Colors.blue,
                ),
              ),
            ),
            const Divider(
              thickness: 1.5,
            ),
          ],
        ),
      ),
    );
  }
}
