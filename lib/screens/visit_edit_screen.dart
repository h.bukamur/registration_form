import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:registration_form/providers/visit_provider.dart';

import '../entities/visit_model.dart';

class VisitEditScreen extends StatefulWidget {
  VisitEditScreen({Key? key, required this.visit}) : super(key: key);
  Visit visit;

  @override
  State<VisitEditScreen> createState() => _VisitEditScreenState();
}

class _VisitEditScreenState extends State<VisitEditScreen> {
  final TextEditingController nameController = TextEditingController();

  bool _validateName = false;
  late Visit visit;

  @override
  void initState() {
    visit = widget.visit;
    nameController.text = visit.name!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Visit Edit'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextField(
              controller: nameController,
              keyboardType: TextInputType.text,
              onChanged: (value) {
                if (value.isNotEmpty) {
                  setState(() {
                    _validateName = false;
                  });
                } else {
                  setState(() {
                    _validateName = true;
                  });
                }
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                label: const Text('Business Name'),
                errorText:
                    _validateName ? 'Please enter the business name!.' : null,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: double.maxFinite,
              height: 50,
              child: ElevatedButton(
                onPressed: () => update(),
                child: const Text(
                  'Save',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void update() {
    if (nameController.text.isEmpty) {
      setState(() {
        _validateName = true;
      });
      return;
    }
    visit.name = nameController.text;
    Provider.of<VisitProvider>(context, listen: false).update(visit);
    Navigator.of(context).pop();
  }
}
