import 'package:flutter/material.dart';

import '../entities/user.dart';

class RegistrationView extends StatelessWidget {
  const RegistrationView({Key? key, required this.user}) : super(key: key);
  final User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Confirmation'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Full Name: ${user.fullName!}',
            style: const TextStyle(fontSize: 24),),
            const SizedBox(
              height: 20,
            ),
            Text('Phone Number: ${user.phoneNumber!}',
              style: const TextStyle(fontSize: 24),),
            const SizedBox(
              height: 20,
            ),
            Text('Email: ${user.email!}',
              style: const TextStyle(fontSize: 24),),
            const SizedBox(
              height: 20,
            ),
            Text('Gender: ${user.gender.name}',
              style: const TextStyle(fontSize: 24),),
          ],
        ),
      ),
    );
  }
}
