import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateTimeScreen extends StatefulWidget {
  const DateTimeScreen({Key? key}) : super(key: key);

  @override
  State<DateTimeScreen> createState() => _DateTimeScreenState();
}

class _DateTimeScreenState extends State<DateTimeScreen> {
  var dateTimeFormat = DateFormat('yyyy/MM/dd HH:mm');
  var dateFormat = DateFormat('yyyy/MM/dd');
  var timeFormat = DateFormat('HH:mm');

  String dateTimeString = 'Date Time';
  String dateString = 'Date Only';
  String timeString = 'Time Only';

  // var number = NumberFormat('N2');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Date Time Picker Screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: dateOnlyPicker,
              child: Text(
                dateString,
                style: const TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: timeOnlyPicker,
              child: Text(
                timeString,
                style: const TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: dateTimePicker,
              child: Text(
                dateTimeString,
                style: const TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void dateOnlyPicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now().add(
        const Duration(days: -18250),
      ),
      lastDate: DateTime.now().add(const Duration(days: 1825)),
    ).then((value) {
      setState(() {
        dateString = dateFormat.format(value!);
      });
    });
  }

  void timeOnlyPicker() {
    showTimePicker(
      context: context,
      initialTime: const TimeOfDay(hour: 00, minute: 00),
    ).then((value) {
      setState(() {
        timeString = timeFormat.format(DateTime(
            DateTime.now().year,
            DateTime.now().month,
            DateTime.now().day,
            value!.hour,
            value.minute));
      });
    });
  }

  void dateTimePicker() {
    DateTime result;
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now().add(
        const Duration(days: -18250),
      ),
      lastDate: DateTime.now().add(const Duration(days: 1825)),
    ).then((value) {
      result = value!;
      showTimePicker(
        context: context,
        initialTime: const TimeOfDay(hour: 00, minute: 00),
      ).then((value) {
        setState(() {
          dateTimeString = dateTimeFormat.format(
            DateTime(
              result.year,
              result.month,
              result.day,
              value!.hour,
              value.minute,
            ),
          );
        });
      });
    });
  }
}
