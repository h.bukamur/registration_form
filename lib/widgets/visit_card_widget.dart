import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';

import '../entities/visit_model.dart';
import '../screens/visit_edit_screen.dart';

class VisitCard extends StatelessWidget {
  VisitCard({
    super.key,
    required this.visit,
  });

  final Visit visit;
  final format = DateFormat("EEEE dd MMMM yyyy");

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Card(
        elevation: 8,
        shape: RoundedRectangleBorder(
          side: const BorderSide(
            width: 2,
            color: Color(0xFFC2913D),
          ),
          borderRadius: BorderRadius.circular(
            20,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 20,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    visit.name!,
                    style: const TextStyle(
                      fontSize: 16,
                      color: Color(0xFFC2913D),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  RatingBarIndicator(
                    rating: visit.rating!,
                    itemBuilder: (context, index) => const Icon(
                      Icons.star,
                      color: Color(0xFFC2913D),
                    ),
                    itemCount: 5,
                    itemSize: 20.0,
                    unratedColor: Colors.amber,
                    direction: Axis.horizontal,
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                format.format(
                  visit.visitDate!,
                ),
                style: TextStyle(fontSize: 16, color: Colors.grey[700]!),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Directorate: ${visit.directorate!}",
                    style: TextStyle(fontSize: 16, color: Colors.grey[700]!),
                  ),
                  Text(
                    "Municipality: ${visit.municipality!}",
                    style: TextStyle(fontSize: 16, color: Colors.grey[700]!),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              const Divider(
                height: 10,
                thickness: 2.5,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 80,
                      child: InkWell(
                          onTap: () {},
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [
                              Icon(
                                Icons.visibility,
                                color: Color(0xFFC2913D),
                              ),
                              Text(
                                'View',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Color(0xFFC2913D),
                                ),
                              ),
                            ],
                          )),
                    ),
                    SizedBox(
                      // width: 80,
                      child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => VisitEditScreen(
                                  visit: visit,
                                ),
                              ),
                            );
                          },
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [
                              Icon(
                                Icons.create_new_folder_outlined,
                                color: Color(0xFFC2913D),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                'Edit',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Color(0xFFC2913D),
                                ),
                              ),
                            ],
                          )),
                    ),
                    SizedBox(
                      width: 80,
                      child: InkWell(
                          onTap: () {},
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [
                              Icon(
                                Icons.note_add_outlined,
                                color: Color(0xFFC2913D),
                              ),
                              Text(
                                'Notice',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Color(0xFFC2913D),
                                ),
                              ),
                            ],
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
