import 'package:flutter/material.dart';

class SettingsDialog {
  TextEditingController secretKeyController = TextEditingController();
  TextEditingController apiKeyController = TextEditingController();
  TextEditingController senderNameController = TextEditingController();

  showSettingsDialog({
    required context,
    required Function() function,
  }) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: const Text(
              'Settings Dialog',
            ),
            actions: [
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Cancel',
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  function();
                },
                child: const Text(
                  'Save',
                ),
              ),
            ],
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextField(
                  controller: apiKeyController,
                  textDirection: TextDirection.ltr,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    label: const Text(
                      "API Key",
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: secretKeyController,
                  textDirection: TextDirection.ltr,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    label: const Text(
                      "Secret Key",
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: senderNameController,
                  textDirection: TextDirection.ltr,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    label: const Text(
                      "Sender Name",
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  confirmationDialog(
      {required BuildContext context,
      required String title,
      required String data}) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Row(
              children: [
                const Icon(
                  Icons.confirmation_number_outlined,
                  color: Colors.blue,
                ),
                Text(title),
              ],
            ),
            content: Text(data),
            actions: [
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("Close"),
              ),
            ],
          );
        });
  }
}
