import 'package:flutter/material.dart';

import '../constant/event_status.dart';
import '../entities/visit_model.dart';

class VisitProvider extends ChangeNotifier {
  final List<Visit> _visitsInitial = [
    Visit(
      id: 1,
      name: "Abunawas Company",
      directorate: "Tripoli",
      municipality: "Tripoli",
      rating: 5,
      visitDate: DateTime(2023, 3, 27),
    ),
    Visit(
      id: 2,
      name: "Google Company",
      directorate: "Tripoli",
      municipality: "Tripoli",
      rating: 4,
      visitDate: DateTime(2023, 3, 28),
    ),
    Visit(
      id: 3,
      name: "Apple Company",
      directorate: "Tripoli",
      municipality: "Tripoli",
      rating: 4.5,
      visitDate: DateTime(2023, 3, 29),
    ),
    Visit(
      id: 4,
      name: "Microsoft Company",
      directorate: "Tripoli",
      municipality: "Tripoli",
      rating: 4.25,
      visitDate: DateTime(2023, 3, 30),
    )
  ];

  Visit _visit = Visit();

  List<Visit> _visits = [];

  Visit get visit => _visit;

  void setVisit(Visit visit) {
    _visit = visit;
  }

  List<Visit> get visits => _visits;

  void setVisits(List<Visit> visits) {
    _visits = visits;
  }

  EventStatus _status = EventStatus.initial;

  EventStatus get status => _status;

  void setStatus(EventStatus status) {
    _status = status;
  }

  Future<void> getAll() async {
    _status = EventStatus.loading;
    await Future.delayed(const Duration(seconds: 5));
    _visits = _visitsInitial;
    _status = EventStatus.complete;
    notifyListeners();
  }

  void update(Visit visit) {
    int index = _visits
        .indexOf(_visits.where((e) => e.id == visit.id).first);
    _visits[index] = visit;
    notifyListeners();
  }
}
